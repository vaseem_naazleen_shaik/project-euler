'''
Problem 2: 
By considering the terms in the Fibonacci sequence whose values do not exceed four million,
find the sum of the even-valued terms.
'''

# solution:
last_fib, fib, next_fib = 1, 2, 0
sum_ = 0
while fib < 4000000:
    if fib % 2 == 0:
        sum_ += fib
        
    next_fib = fib + last_fib
    last_fib = fib
    fib = next_fib
    
    
print(sum_)

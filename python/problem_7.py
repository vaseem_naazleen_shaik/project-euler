# Problem 7: What is the 10 001st prime number?

#Solution:

def is_prime(n : int) -> bool:
    if n in [2, 3, 5, 7, 9]:
        return True
    elif n <= 1:
        return False
    elif n % 2 == 0 or n % 3 == 0:
        return False
    else:
         r = 9
         while r * r < n :
             if n % r == 0:
                 return True
             r += 2
         return False

def prime_10001():
    i = 0
    count = 0
    while count <= 10000:
        if is_prime(i):
            count += 1
        i += 1
    return i - 1
        

        
             
print(prime_10001())

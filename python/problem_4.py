#Problem - 4: Find the largest palindrome made from the product of two 3-digit numbers.

# solution:

def is_palindrome(n : int) -> bool:
    return str(n) == str(n)[::-1]

def largest_palin_product_3_digit_nums() -> int:
    return max([i * j for i in range(100, 1000) for j in range(100, 1000) if is_palindrome(i * j)])

print(largest_palin_product_3_digit_nums())

# Problem - 5: What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

#explanation:
# LCM(x, y) = x * y / GCD(x, y)
# LCM(k1, k2, ..., k_m) = LCM(...(LCM(LCM(k1, k2), k3)...), k_m).


# solution :

from math import gcd

def evenly_divisible_till20():
    answer = 1
    for i in range(1, 21):
        answer *= i // gcd(i, answer)
    return answer
            
print(evenly_divisible_till20())

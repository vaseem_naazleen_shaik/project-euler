#Problem 9:
'''
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.'''

#Solution:
    
PERIMETER = 1000

def pyth_triplet() -> int:
    for i in range(1, PERIMETER + 1):
        for j in range(1, PERIMETER + 1):
            for k in range(1, PERIMETER + 1):
                if i + j + k == 1000 and i ** 2 + j ** 2 == k ** 2:
                    return i * j * k

print(pyth_triplet())

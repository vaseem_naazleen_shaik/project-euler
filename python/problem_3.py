#Problem 3: What is the largest prime factor of the number 600851475143 ?

'''
Explanation :

-> every number > 1 can be expressed as product of primes.
-> if we take n and repeatedly divide out its smallest factor
   then the last factor (is a prime itself)
-> so the last factor would be largest prime factor of given number
'''

#sol:

from math import sqrt

number = 600851475143

def smallest_prime_factor(n : int) -> int:
    assert n >= 2
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return i
    return n

def largest_prime_factor(n : int) -> int:
    spf = smallest_prime_factor(n)
    while n > spf:
        spf = smallest_prime_factor(n)
        n //= spf
    return spf

print(largest_prime_factor(number))

#problem 6 : Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

'''
Formulas:
-> sum of first n natural nums = n * (n + 1) / 2
-> sum of squares of first n natural nums = n * (n + 1) * (2 * n + 1) / 6
'''

#Solution:

def sum_of_nums(n : int) -> int:
    return n * (n + 1) // 2

def sum_of_sqrs(n : int) -> int:
    return n * (n + 1) * (2 * n + 1) // 6

def difference():
    sq_of_sum = pow(sum_of_nums(100), 2)
    sum_of_sq = sum_of_sqrs(100)
    return sq_of_sum - sum_of_sq

print(difference())

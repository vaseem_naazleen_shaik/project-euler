# Problem 10 : Find the sum of all the primes below two million.

# Solution:

def is_prime(n : int):
    if n % 2 == 0:
        return False
    else:
        r = 3
        while r * r  <= n:
            if n % r == 0:
                return False
            r += 2
        return True

def sum_nprimes(n : int) -> int:
    primes = filter(is_prime, list(range(3, 2000000, 2)))

    return sum(primes) + 2      // add 2 bcuz we started from 3
    
print(sum_nprimes(2000000))     # Answer : 142913828922

#Problem 1 : Find the sum of all the multiples of 3 or 5 below 1000.

# sol 1:
lst = [i for i in range(1001) if i % 3 == 0 or i % 5 == 0]
print(sum(lst))

# sol 2:
lst3 = range(0,1001, 3)
lst5 = range(0,1001, 5)
lst15 = range(0,1001, 15)

sum_ = sum(lst3) + sum(lst5) - sum(lst15)
print(sum_)
